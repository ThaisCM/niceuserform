package com.itb.niceuserform.retrofit;

public class UserSession {
    String authToken;
    String error;

    public String getAuthToken() {
        return authToken;
    }

    public String getError() {
        return error;
    }

    public boolean isLogged(){
        return authToken!=null;
    }
}
