package com.itb.niceuserform.views;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.checkbox.MaterialCheckBox;
import com.itb.niceuserform.R;

public class WelcomeFragment extends Fragment {

    private WelcomeViewModel mViewModel;
    MaterialButton btnLogin;
    MaterialButton btnRegister;

    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnLogin = view.findViewById(R.id.btnWelcomeLogin);
        btnRegister = view.findViewById(R.id.btnWelcomeRegister);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WelcomeViewModel.class);
        // TODO: Use the ViewModel
        btnLogin.setOnClickListener(this::onLoginButtonClicked);
        btnRegister.setOnClickListener(this::onRegisterButtonClicked);
    }

    private void onRegisterButtonClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcomeFragment_to_registerFragment);
    }

    private void onLoginButtonClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_welcomeFragment_to_loginFragment);
    }

}
