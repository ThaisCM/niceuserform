package com.itb.niceuserform.views;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProviders;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.nfc.tech.NfcA;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavGraphNavigator;
import androidx.navigation.Navigation;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.itb.niceuserform.R;

public class RegisterFragment extends Fragment {

    private RegisterViewModel mViewModel;
    private MaterialButton btnLogin;
    private MaterialButton btnRegister;
    private TextInputLayout tilUsername;
    private TextInputLayout tilPassword;
    private TextInputLayout tilPassword2;
    private TextInputLayout tilEmail;
    private Dialog dialog;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        btnLogin = view.findViewById(R.id.btnRLogin);
        btnRegister = view.findViewById(R.id.btnRRegister);
        tilUsername = view.findViewById(R.id.tilRUsername);
        tilPassword = view.findViewById(R.id.tilRPassword);
        tilPassword2 = view.findViewById(R.id.tilRPAssword2);
        tilEmail = view.findViewById(R.id.tilREmail);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        // TODO: Use the ViewModel
        btnLogin.setOnClickListener(this::onLoginButtonClicked);
        btnRegister.setOnClickListener(this::onRegisterButtonClicked);
        mViewModel.getLoading().observe(getViewLifecycleOwner(), this::onLoadingChanged);
        mViewModel.getLogged().observe(getViewLifecycleOwner(), this::onLoggedChanged);
        mViewModel.getError().observe(getViewLifecycleOwner(), this::onErrorChanged);
    }

    private void onErrorChanged(String error) {
        Snackbar.make(getView(), error, Snackbar.LENGTH_LONG).show();
    }

    private void onLoggedChanged(Boolean logged) {
        if(logged){
            Navigation.findNavController(getView()).navigate(R.id.action_registerFragment_to_userLoggedFragment);
        }
    }

    private void onLoadingChanged(Boolean loading) {
        if (loading){
            dialog = ProgressDialog.show(getContext(), "Loading", "Loading...", true);
        } else {
            if(dialog != null) dialog.dismiss();
        }
    }

    private void onRegisterButtonClicked(View view) {
        if(validateForm()){
            mViewModel.register(tilUsername.getEditText().getText().toString().trim());
        }
    }

    private boolean validateForm(){
        boolean valid = true;
        valid = setErrorIfIsEmpty(tilUsername) && valid;
        valid = setErrorIfIsEmpty(tilPassword) & setErrorIfIsEmpty(tilPassword2) && setErrorIfPasswordsNotMatch(tilPassword,tilPassword2) && valid;
        valid = setErrorIfIsEmpty(tilEmail) && setErrorIfEmailWrongFormat(tilEmail) && valid;
        return valid;
    }

    private boolean setErrorIfIsEmpty(TextInputLayout til) {
        til.setError("");
        if (til.getEditText().getText().toString().isEmpty()){
            til.setError(getString(R.string.required_field));
            scrollTo(til);
            return false;
        }
        return true;

    }

    private boolean setErrorIfPasswordsNotMatch(TextInputLayout t1, TextInputLayout t2){
        if(!t1.getEditText().getText().toString().isEmpty() | !t2.getEditText().getText().toString().isEmpty()) {
            t1.setError("");
            t2.setError("");
        }
        if (!t1.getEditText().getText().toString().equals(t2.getEditText().getText().toString())) {
            t1.setError(getString(R.string.pass_not_match));
            t2.setError(getString(R.string.pass_not_match));
            scrollTo(t1);
            scrollTo(t2);
            return false;
        }
        return true;

    }

    private boolean setErrorIfEmailWrongFormat(TextInputLayout til){
        til.setError("");
        if(!Patterns.EMAIL_ADDRESS.matcher(til.getEditText().getText().toString()).matches()){
            til.setError(getString(R.string.wrong_email_format));
            scrollTo(til);
            return false;
        }
        return true;
    }

    private void onLoginButtonClicked(View view) {
        Navigation.findNavController(view).navigate(R.id.action_registerFragment_to_loginFragment);
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView,targetView);
    }

}
